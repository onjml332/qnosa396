#第一财经JJ诚信金商eyk
#### 介绍
JJ诚信金商【溦:844825】，JJ诚信金商【溦:844825】，　　那一只鸡试图飞过那个道路的缺口，结果落入了水中，随着从稻田冲下的水流迅速地往河边冲去。鸡在水中徒劳地挣扎着，扑扇着翅膀，想往无水的边缘靠近。它一次次努力着，一次次地被水重新冲到了中央。它惊叫着，一根断落并飘浮着的树枝救了它，它的爪抓住了树枝，树枝在两块石头之间停止了飘浮。
　　现在是作家出少年的时代，安庆市十七岁的少年陈进近日由花城出版社推出长篇小说《固都》，且印数不错，大有赶韩寒《三重门》之势。与韩寒不同的是，陈进在自己的文学天才之外，语言上更显个性与风采，“流畅的文字中可见钱钟书的遗风，又依稀现出张恨水的身影”（出版社评论语）。在两位大家的身影间，一个少年的飒爽风姿隐约可见。
　　“你既是如许聪慧，”她不怀好心地说，“就去把我屋前的铁树锯成两半吧。”

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/